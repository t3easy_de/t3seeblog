<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'T3see.' . $_EXTKEY,
	'Main',
	array(
		'Blog' => 'list, show, edit',
		'Post' => 'show',

	),
	// non-cacheable actions
	array(
		'Blog' => '',
		'Post' => '',

	)
);

?>